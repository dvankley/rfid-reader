import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by dvankley on 3/2/16.
 */
public class WebClientManager {
	ExecutorService poster = Executors.newSingleThreadExecutor();

	public void handleNewCardUid(String uidHash) {
		poster.submit( () -> {
			try {
                RfidReader.logger.info("Posting new user hash: " + uidHash);
				URIBuilder builder = new URIBuilder();
				builder.setScheme("http")
						.setHost("pardot-pingpong.herokuapp.com")
						.setPath("/api/rooms/1/players/add")
						.setParameter("uidHash", uidHash);
				String response = Request.Get(builder.build().toASCIIString())
						.execute()
						.returnContent()
						.asString();
//                RfidReader.logger.info("New lap time post response: " + response);
			} catch (Exception e) {
				RfidReader.logger.severe(e.toString());
			}
		});
	}

	public void shutdown() {
		this.poster.shutdown();
	}
}
