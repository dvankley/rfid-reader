import org.apache.commons.codec.digest.DigestUtils;

import javax.smartcardio.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by dvankley on 4/5/16.
 */
public class SmartCardManager {
	public final ExecutorService cardPollExecutor = Executors.newSingleThreadExecutor();
	public boolean shutdown = false;

	public SmartCardManager(WebClientManager webClient)
	{
		this.cardPollExecutor.submit(() -> {
			try {
				// Display the list of terminals
				TerminalFactory factory = TerminalFactory.getDefault();
				List<CardTerminal> terminals = factory.terminals().list();
				RfidReader.logger.info("Terminals: " + terminals);

				// Use the first terminal
				CardTerminal terminal = terminals.get(0);

				// Keep looking for card transactions
				while (!this.shutdown) {
					// I will wait forever for you
					terminal.waitForCardPresent(0);

					try {
						// Connect with the card
						Card card = terminal.connect("*");
						RfidReader.logger.info("card: " + card + ", ATR: " + this.byteArrayToHexString(card.getATR().getBytes()));
						CardChannel channel = card.getBasicChannel();

						// Send get UID command
						ResponseAPDU answer = channel.transmit(new CommandAPDU(0xFF, 0xCA, 0x00, 0x00, new byte[0], 0x08));
						byte[] uidBytes = answer.getData();
						RfidReader.logger.info("UID: " + this.byteArrayToHexString(uidBytes));

						// Hash UID
						String hash = DigestUtils.sha256Hex(uidBytes);

						RfidReader.logger.info("Hashed UID as: " + hash);

						// Async post data to server
						webClient.handleNewCardUid(hash);

						// Wait until the card is gone
						terminal.waitForCardAbsent(0);

						// Disconnect the card
						card.disconnect(false);
					} catch (CardException e) {
						if (e.getMessage().equals("connect() failed")) {
							RfidReader.logger.info("Card disconnect");
						} else {
							throw e;
						}
					}
				}
			} catch(Exception e) {
				RfidReader.logger.severe("RFID reader exception: " + e.toString());
			}
		});
	}

	public String byteArrayToHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (byte aB : b) {
			int v = aB & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}
}
