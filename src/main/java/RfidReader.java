import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by dvankley on 4/6/16.
 */
public class RfidReader {
	public static final Environment env = Environment.DEV;
	public static Logger logger;

	public static void main(String[] args) {
		RfidReader.logger = Logger.getLogger("rfid-logger");
		SimpleFormatter formatter = new SimpleFormatter();

		try {
			FileHandler fh = new FileHandler("rfid-reader.log");
			logger.addHandler(fh);
			fh.setFormatter(formatter);

		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}

		logger.info("Main logger initialized");

		// Build managers
		WebClientManager client = new WebClientManager();

		SmartCardManager smartCard = new SmartCardManager(client);

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			RfidReader.logger.info("Shutting down moto timer");
			client.shutdown();
			smartCard.shutdown = true;
		}));
	}
}
